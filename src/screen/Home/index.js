import React, { Component } from 'react';
import { FlatList, Image, StyleSheet, Keyboard, View, StatusBar, TouchableOpacity } from 'react-native';
import {
  Container, Header, Tab, Tabs, ScrollableTab,
  Card, CardItem, Body, Text, Button, Item, Icon, Input,
  Spinner
} from 'native-base';

import { observer, inject } from 'mobx-react'

class index extends Component {

  state = {
    search: false,
    searchText: '',
    page: 1,
  }

  renderItem = ({ item }) => {

    const { navigate } = this.props.navigation;

    return (
      <Card style={[styles.bgDark, { borderColor: '#102027' }]}>
        <CardItem style={[styles.bgDark]}>
          <Body>
            <TouchableOpacity onPress={() => navigate('Detail',{item})}>
              <Text style={{ color: '#ebedee' }} >{item.title}</Text>
            </TouchableOpacity>
          </Body>
        </CardItem>
        <CardItem cardBody>
          <Image source={{ uri: item.urlToImage }} style={{ height: 200, width: null, flex: 1 }} />
        </CardItem>
      </Card>
    )

  }

  changeCategory = item => {

    switch (item) {
      case 0:
        this.load("general");
        break;
      case 1:
        this.load("sport");
        break;
      case 2:
        this.load("health");
        break;
      case 3:
        this.load("business");
        break;
      case 4:
        this.load("entertainment");
        break;
      case 5:
        this.load("technology");
        break;
      default:
        this.load("general");
        break;
    }

  }

  load = category => {

    this.setState({ page: 1, searchText: '', search: false });
    setTimeout(() => {
      //console.log(this.state.page+"---");
      this.props.haberStore.newsLoading = true;
      this.props.haberStore.category = category;
      this.props.haberStore.getNews(this.state.page);

    }, 200);

  }

  seacrh = text => {
    this.setState({ searchText: text });
  }

  searchButton = () => {

    if (this.state.searchText != "") {
      this.setState({ search: true })
    }
    else {
      this.setState({ search: false })
      this.setState({ page: 1 })
    }

    Keyboard.dismiss();
    this.props.haberStore.newsLoading = true;
    this.props.haberStore.searchNews(this.state.searchText);
  }

  onRefresh = () => {

    if (this.state.search) {
      this.searchButton();
    }
    else {
      this.setState({ page: 1 });
      this.props.haberStore.getNews(this.state.page);
    }

  }

  getNews = () => {
    if (!this.state.search) {
      let page = this.state.page + 1;
      this.setState({ page });
      //console.log(page);
      this.props.haberStore.getNews(page);
    }

  }

  getList = () => {

    const { newsLoading, news } = this.props.haberStore

    if (newsLoading) {
      return (
        <View style={{ flex: 1, backgroundColor: '#000a12' }}>
          <Spinner color='#4fc3f7' />
        </View>

      )
    }
    else {
      return (
        <FlatList
          style={[styles.list]}
          data={news}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index}
          refreshing={false}
          onRefresh={() => this.onRefresh()}
          onEndReached={this.getNews}

        />
      )
    }

  }

  componentDidMount() {
    this.props.haberStore.category = "general";
    this.props.haberStore.getNews(this.state.page);
  }

  render() {
    return (

      <Container style={{ backgroundColor: '#000a12' }}>

        <Header searchBar rounded style={{ backgroundColor: '#102027', }}>
          <StatusBar backgroundColor="#000a12" />
          <Item style={{ borderRadius: 5, backgroundColor: '#102027', }}>
            <Input
              placeholder="Haberlerde Ara"
              onChangeText={(text) => { this.seacrh(text); }}
              onSubmitEditing={this.searchButton}
              style={{ color: '#ebedee' }}
              placeholderTextColor="#ebedee"
              value={this.state.searchText}
            />
            <Icon name="ios-search" style={{ color: "#ebedee" }} onPress={this.searchButton} />
          </Item>
          <Button transparent >
            <Text>Search</Text>
          </Button>
        </Header>

        <Tabs renderTabBar={() => <ScrollableTab style={{ borderWidth: 0, borderColor: '#4fc3f7' }} />}
          onChangeTab={({ i }) => this.changeCategory(i)}
          tabBarUnderlineStyle={{ backgroundColor: '#4fc3f7', }}
        >

          <Tab
            heading="GÜNCEL"
            tabStyle={[styles.tabStyle]}
            activeTabStyle={[styles.activeTabStyle]}
            textStyle={[styles.textStyle]}
            activeTextStyle={[styles.activeTextStyle]}
          >
            {this.getList()}
          </Tab>

          <Tab
            heading="SPOR"
            tabStyle={[styles.tabStyle]}
            activeTabStyle={[styles.activeTabStyle]}
            textStyle={[styles.textStyle]}
            activeTextStyle={[styles.activeTextStyle]}
          >
            {this.getList()}
          </Tab>

          <Tab
            heading="SAĞLIK"
            tabStyle={[styles.tabStyle]}
            activeTabStyle={[styles.activeTabStyle]}
            textStyle={[styles.textStyle]}
            activeTextStyle={[styles.activeTextStyle]}
          >
            {this.getList()}
          </Tab>

          <Tab
            heading="İŞ"
            tabStyle={[styles.tabStyle]}
            activeTabStyle={[styles.activeTabStyle]}
            textStyle={[styles.textStyle]}
            activeTextStyle={[styles.activeTextStyle]}
          >
            {this.getList()}
          </Tab>

          <Tab
            heading="EĞLENCE"
            tabStyle={[styles.tabStyle]}
            activeTabStyle={[styles.activeTabStyle]}
            textStyle={[styles.textStyle]}
            activeTextStyle={[styles.activeTextStyle]}
          >
            {this.getList()}
          </Tab>

          <Tab
            heading="TEKNOLOJİ"
            tabStyle={[styles.tabStyle]}
            activeTabStyle={[styles.activeTabStyle]}
            textStyle={[styles.textStyle]}
            activeTextStyle={[styles.activeTextStyle]}
          >
            {this.getList()}
          </Tab>

        </Tabs>



      </Container>

    );
  }
}



const styles = StyleSheet.create({
  main: {
    backgroundColor: '#000a12',
  },
  list: {
    padding: 8,
    backgroundColor: '#000a12',
  },
  tab: {
    backgroundColor: '#102027'
  },
  tabStyle: {
    backgroundColor: '#102027',
  },
  activeTabStyle: {
    backgroundColor: '#102027'
  },
  textStyle: {
    color: '#ebedee'
  },
  activeTextStyle: {
    color: '#4fc3f7'
  },
  bgDark: {
    backgroundColor: '#102027',
  }

})

export default inject("haberStore")(observer(index))