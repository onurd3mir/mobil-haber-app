import React, { Component } from 'react'
import { StyleSheet, StatusBar } from 'react-native';
import { WebView } from 'react-native-webview';
import { Container, Header, Body, Icon, Left, Title } from 'native-base';

export class index extends Component {

    render() {

        const { route, navigation } = this.props;
        const { item } = route.params;

        return (
            <Container>
                <Header style={{ backgroundColor: '#102027' }}>
                    <StatusBar backgroundColor="#000a12" />
                    <Left>
                        <Icon name='arrow-back' style={[styles.contentColor]} onPress={() => navigation.goBack()} />
                    </Left>
                    <Body>
                        <Title style={[styles.contentColor]} >{item.author}</Title>
                    </Body>
                </Header>

                <WebView
                    source={{ uri: item.url }}
                />
            </Container>

        )
    }
}

const styles = StyleSheet.create({
    contentColor: {
        color: '#ebedee',
        textAlign: 'justify'
    }
})

export default index
