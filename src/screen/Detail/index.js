import React, { Component } from 'react';
import { Image, StyleSheet, View, StatusBar, TouchableOpacity } from 'react-native';
import { Container, Header, Body, Text, Icon, Left, Content, Title } from 'native-base';

class index extends Component {

  goNews = () => {
    const { route, navigation } = this.props;
    const { item } = route.params;
    //console.log(item.url);
    navigation.navigate("DetailView", { item });
  }

  render() {

    const { route, navigation } = this.props;
    const { item } = route.params;

    return (
      <Container style={{ backgroundColor: '#000a12' }}>

        <Header style={{ backgroundColor: '#102027' }}>
          <StatusBar backgroundColor="#000a12" />
          <Left>
            <Icon name='arrow-back' style={[styles.contentColor]} onPress={() => navigation.goBack()} />
          </Left>
          <Body>
            <Title style={[styles.contentColor]} >{item.author}</Title>
          </Body>
        </Header>

        <Content>

          <View style={{ paddingVertical: 10, paddingHorizontal: 15 }} >
            <Text style={[styles.contentColor]}>{item.title}</Text>
          </View>

          <Image source={{ uri: item.urlToImage }} style={{ height: 200 }} />

          <View style={{ paddingVertical: 10, paddingHorizontal: 15 }}>
            <Text style={[styles.contentColor]}>
              {item.description}
            </Text>
          </View>

          <TouchableOpacity style={{ paddingVertical: 10, paddingHorizontal: 15 }} onPress={() => this.goNews()}>
            <Text style={[styles.contentColor]}>
              Haberi Görüntelemek İçin Tıklayınız
            </Text>
          </TouchableOpacity>

          {/* <Text style={{ color: '#fff' }}>
            {item.content}
          </Text> */}

        </Content>

      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentColor: {
    color: '#ebedee',
    textAlign: 'justify'
  }
})

export default index;
