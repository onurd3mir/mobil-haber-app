import { observable, action, computed, runInAction, decorate } from 'mobx'

class haberStore {

    apiKey = "e0b43a3fc6854e7e90749cdc8c78dee6";
    news = [];
    newsLoading = true;
    category = "";

    async getNews(page) {

        let url = `https://newsapi.org/v2/top-headlines?country=tr&apiKey=${this.apiKey}&pagesize=10`;
        url += `&page=${page}`;
        if (this.category != "") url += `&category=${this.category}`;

        const response = await fetch(url);
        const data = await response.json();

        if (data.articles.length > 0) {

            if(page==1){
                runInAction(() => {
                    this.news = data.articles;
                    this.newsLoading = false;
                })
            }
            else{
                console.log(`${this.category}-${page}`);
                runInAction(()=>{
                    data.articles.forEach(i => {
                        this.news.push(i);
                    });
                })

            } 
        }
        else{
            this.newsLoading = false;
        }
    }


    async searchNews(text) {

        let url = `https://newsapi.org/v2/top-headlines?country=tr&apiKey=${this.apiKey}&category=${this.category}`;
        url += `&q=${text}`;

        const response = await fetch(url);
        const data = await response.json();

        if (data.articles.length > 0) {

            runInAction(() => {
                this.news = data.articles;
                this.newsLoading = false;
            })
        }
        else {
            this.newsLoading = false;
        }
    }

}

haberStore = decorate(haberStore, {
    news: observable,
    newsLoading: observable,
    category: observable,
    getNews: action,
    searchNews: action
})

export default new haberStore();