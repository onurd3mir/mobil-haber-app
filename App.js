import React, { Component } from 'react'

import {Provider} from 'mobx-react'
import store from './src/store'

import Route from './src/Route'

export class App extends Component {
  render() {
    return (
     <Provider {...store}>
        <Route />
     </Provider>
    )
  }
}

export default App
